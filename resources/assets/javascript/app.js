$(window).load(function () {
    $('input.empty').val('');
    checkAlerts();
    setDeletePage();
    $("#manoBold").on('click', function () {
        document.location = '/login';
    });
    $("#logoutUser").on("click", function () {
        document.location = '/logout';
    });
    $("#classList").on("change", function () {
        var classId = $(this).find(':selected').data('class-id');
        if (typeof studentSignalPoints != 'undefined') {
            studentSignalPoints.canvas.remove();
            $('.chart').append('<canvas id="signalPointsChart"></canvas>')
        }
        makeSignalPointsChart(classId);
    });

    $('#formAddClass').on('submit', function (e) {
        e.preventDefault();
        $(this).find('.required').each(function () {
            if (!this.value) {
                $(this).css('border', '1px solid red');
                $(this).parent().find('.alertHide').show();
            } else if (this.value) {
                $(this).css('border', '1px solid grey');
                $(this).parent().find('.alertHide').hide();
                loadingGif();
                $.ajax({
                    type: "POST",
                    url: '/admin/addClass',
                    data: $(this).serialize(),
                    success: function (result) {
                        if (result == true) {
                            localStorage.setItem('addClass', true);
                            loadingGif();
                            location.reload();
                        } else {
                            localStorage.setItem('addClass', false);
                            loadingGif();
                            location.reload();
                        }
                    }
                });
            }
        });
    });

    $('#studentsImportForm').on('submit', function () {
        var form = $(this);
        loadingGif();
        $.ajax({
            type: "POST",
            url: '/upload/importStudents',
            data: form.serialize(),
            success: function () {
            }
        });
    });

    $('#addAccountForm').on('submit', function (e) {
        e.preventDefault();
        var form = $(this);
        var empty = form.find(".required").filter(function () {
            return this.value === "";
        });
        if (!empty.length) {
            loadingGif();
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize()
            }).done(function (result) {
                if (result !== false) {
                    if (result == 'password') {
                        localStorage.setItem('addAccountPassword', 'false');
                        location.reload();
                    } else {
                        localStorage.setItem('addAccount', 'true');
                        location.reload();
                    }
                } else {
                    localStorage.setItem('addAccount', 'false');
                    location.reload();
                }
            });
        } else {
            form.find('.required').each(function () {
                if (!this.value) {
                    $(this).css('border', '1px solid red');
                    $(this).parent().find('.alertHide').show();
                } else if (this.value) {
                    $(this).css('border', '1px solid grey');
                    $(this).parent().find('.alertHide').hide();
                }
            });
        }
    });

    $('.startLoadingButton').on('click', function () {
        loadingGif();
    });

    if ($('.classes-overview').length) {
        $(document).ready(function () {
            makeClassesChart();
        });
    }

    $(function () {
        $.ajax({
            url: ' /searches/dataSearch'
        }).done(function (data) {
            var ovNumbers = data.ovNumbers;
            var classNames = data.classes;
            var names = data.names;
            $('.sidebar-form').on("mouseenter keyup", function () {
                var divElement = $('#dropdown-content');
                divElement.hide();
                divElement.html('');
                var currentVal = $('#searchBar').val().toLowerCase();
                if (currentVal !== '') {
                    divElement.show();
                    for (var i = 0; i < ovNumbers.length; i++) {
                        if (ovNumbers[i].OV_Number.toLowerCase().indexOf(currentVal) > -1) {
                            divElement.append('<p class="noPage">' + ovNumbers[i].OV_Number + '</p>');
                        }
                    }
                    for (var i = 0; i < names.length; i++) {
                        if (names[i].name.toLowerCase().indexOf(currentVal) > -1) {
                            divElement.append('<p class="noPage">' + names[i].name + '</p>');
                        }
                    }
                    for (var i = 0; i < classNames.length; i++) {
                        if (classNames[i].class_code.toLowerCase().indexOf(currentVal) > -1) {
                            divElement.append('<p data-class-id=' + classNames[i].id + ' class="classCode">' + classNames[i].class_code + '</p>');
                        }
                    }
                }
                $('.sidebar-form').on("mouseleave", function () {
                    divElement.hide();
                    $('#dropdown-content').html('');
                });

                $(".classCode").on('click', function () {
                    var classCode = $(this).data('class-id');
                    loadingGif();
                    document.location = '/classes/overview/' + classCode;
                });
                $(".noPage").on('click', function () {
                    loadingGif();
                    document.location = '/classes/overview/';
                });
            });
        });
    });

    $('.deleteStudents').on('click', function () {
        if (studentsToDelete == '') {
            alert('Selecteer op zijn minst 1 student om te verwijderen.');
        } else {
            if (confirm('Weet je het zeker dat je deze student(en) wilt verwijderen?')) {
                $(this).prev('span.text').remove();
                loadingGif();
                $.ajax({
                    type: "GET",
                    url: '/admin/deleteStudents',
                    data: {'studentsToDelete': studentsToDelete}
                }).done(function (result) {
                    setDeletePage();
                    if (result != false) {
                        localStorage.setItem('deleteStudents', 'true');
                        location.reload();
                    } else {
                        localStorage.setItem('deleteStudents', 'false');
                        location.reload();
                    }
                });
            }
        }
    });
});

function setDeletePage() {
    $('.checkboxStudents').find('input[type=checkbox]:checked').prop('checked', false);
    $('.checkboxStudents').find('div').removeClass('checked');
}

var studentsToDelete = [];
$(document).on('ifChanged', '.studentRow', function () {
    var studentId = $(this).data('student-id');
    var existedId = studentsToDelete.indexOf(studentId);
    if (existedId === -1) {
        studentsToDelete.push(studentId);
    } else {
        studentsToDelete.splice(existedId, 1);
    }
});

function loadingGif() {
    var container = $('.container');
    var contentBackground = $('.contentBackground');
    $('.loadingGif').show();
    container.addClass('blurBackground');
    contentBackground.addClass('blurBackground');
    container.css('pointer-events', 'none');
    contentBackground.css('pointer-events', 'none');
}

function loadingGifBox() {
    var boxBody = $('.chart-loading');
    $('.loadingGifBox').show();
    boxBody.addClass('blurBackground');
}

function removeLoadingGifBox() {
    var boxBody = $('.chart-loading');
    $('.loadingGifBox').hide();
    boxBody.removeClass('blurBackground');
}

function checkAlerts() {
    var addClass = localStorage.getItem('addClass');
    var addAccount = localStorage.getItem('addAccount');
    var addAccountPassword = localStorage.getItem('addAccountPassword');
    var roleAlert = $('.roleSuccessAlert');
    var deleteStudents = localStorage.getItem('deleteStudents');

    if (addAccount == 'true') {
        $('.accountSuccessAlert').fadeIn().delay(2000).fadeOut('slow');
        localStorage.setItem('addAccount', null);
    } else if (addAccount == 'false') {
        $('.accountErrorAlert').fadeIn().delay(2000).fadeOut('slow');
        localStorage.setItem('addAccount', null);
    }

    if (addAccountPassword == 'false') {
        $('.accountPasswordErrorAlert').fadeIn().delay(2000).fadeOut('slow');
        localStorage.setItem('addAccountPassword', null);
    }

    if (addClass == 'true') {
        $('.classSuccessAlert').fadeIn().delay(2000).fadeOut('slow');
        localStorage.setItem('addClass', null)
    } else if (addClass == 'false') {
        $('.classErrorAlert').fadeIn().delay(2000).fadeOut('slow');
        localStorage.setItem('addClass', null)
    }
    if (roleAlert.length) {
        roleAlert.delay(3000).fadeOut('slow');
    }

    if (deleteStudents == 'true') {
        $('.deleteStudentsSuccessAlert').fadeIn().delay(2000).fadeOut('slow');
        localStorage.setItem('deleteStudents', null);
    } else if (deleteStudents == 'false') {
        $('.deleteStudentsErrorAlert').fadeIn().delay(2000).fadeOut('slow');
        localStorage.setItem('deleteStudents', null);
    }
}

$(function () {
    $('input.studentRow, input.rememberMeBox').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
});

$(function () {
    var languageDataTable =
        'Aantal resultaten <select>' +
        '<option value="10">10</option>' +
        '<option value="25">25</option>' +
        '<option value="50">50</option>' +
        '<option value="100">100</option>' +
        '<option value="-1">Alles</option>' +
        '</select>';

    $('#studentsTable, #problemsTable, #classTable').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "columnDefs": [
            {orderable: false, targets: 'noSort'},
            {type: 'date-eu', targets: 'dateSort'}
        ],
        "aaSorting": [],
        "language": {
            "lengthMenu": languageDataTable,
            "sSearch": "Zoeken: ",
            "sInfoEmpty": "Er worden 0 tot 0 van de 0 resulaten weergegeven",
            "sInfo": "Er worden _START_ tot _END_ van de _TOTAL_ resulaten weergegeven",
            "sInfoFiltered": "(totaal _MAX_ resultaten)",
            "sZeroRecords": "Geen resultaten gevonden",
            paginate: {
                next: '&#8594;',
                previous: '&#8592;'
            }
        }
    });
});