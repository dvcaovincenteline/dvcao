@extends('layouts.master')

@section('content')
    <section class="content contentBackground">
        <div class="row">
            <div class="col-xs-6">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Klassen overzicht</h3>
                    </div>
                    <div class="box-body">
                        <br>
                        <table id="singleTable" class="table table-bordered table-striped classes-overview">
                            <thead>
                            <tr>
                                <th>Klas</th>
                                <th>Aantal leerlingen</th>
                                <th>Aantal signaal punten</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($classInfo as $class)
                                <tr>
                                    <td>
                                        <a href="{{ route('classesOverviewStudents', ['class' => $class['id']]) }}"
                                           class="startLoadingButton">
                                            {{ $class['class_code'] }}
                                        </a>
                                    </td>
                                    <td> {{ $class['students_amount'] }}</td>
                                    <td> {{ $class['signal_points'] }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                @include('includes.loadingGifBox')
                <div class="box chart-loading">
                    <div class="box-header">
                        <h3 class="box-title">Signaalpunten</h3>
                    </div>
                    <div class="box-body">
                        <div class="chart">
                            <canvas class="signalPointsAllClasses" id="signalPointsChart"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection