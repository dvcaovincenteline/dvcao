@extends('layouts.master')

@section('content')
    <section class="content contentBackground">
        <div class="row">
            <div class="col-xs-6">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">{{ $className }} studenten overzicht</h3>
                    </div>
                    <div class="box-body">
                        <br>
                        <table id="classTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Studenten</th>
                                <th>OV Nummer</th>
                                <th class="dateSort">Geboortedatum</th>
                                <th>Signaal punten</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($students as $student)
                                <tr>
                                    <td> {{ $student->name }} </td>
                                    <td> {{ $student->OV_number }} </td>
                                    @if ($student->birthday)
                                        <td>{{ \App\DateTime::formatDutch($student->birthday) }}</td>
                                    @else
                                        <td>-</td>
                                    @endif
                                    <td> {{ $student->signal_points }} </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection