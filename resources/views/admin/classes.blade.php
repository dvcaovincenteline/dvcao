@extends('layouts.master')

@section('content')
    <section class="container">
        <div class="row">
            <div class="col-xs-4">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Klas toevoegen</h3>
                    </div>
                    <div class="box-body">
                        <div class="input-group input-group-sm">
                            <form id="formAddClass">
                                {{ csrf_field() }}
                                <label for="addClass">Klas toevoegen <b class="danger-font--color">*</b></label>
                                <input type="text" id="addClass" name="className" class="form-control required"
                                       placeholder="LPICOxxxxx">
                                <span class="alertHide danger-font--color">Dit veld is verplicht.</span>
                                <button type="submit" id="addClassSubmit" class="btn btn-block btn-primary">
                                    Verzenden
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-8">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Klassen overzicht</h3>
                    </div>
                    <div class="box-body">
                        <br>
                        <table id="classTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Klas</th>
                                <th>Aantal leerlingen</th>
                                <th>Aantal signaal punten</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($classInfo as $class)
                                <tr>
                                    <td>{{ $class['class_code'] }}</td>
                                    <td>{{ $class['students_amount'] }}</td>
                                    <td>{{ $class['signal_points'] }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection