@extends('layouts.master')

@section('content')
    @if(session()->has('message'))
        <div class="alert alert-success alert-dismissible roleSuccessAlert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> Success!</h4>
            {{ session()->get('message') }}
        </div>
    @endif
    <section class="content contentBackground">
        <div class="row">
            <div class="col-xs-5">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Studenten importeren</h3>
                    </div>
                    <form id="studentsImportForm" action="{{ route('importStudentsUpload') }}" method="POST"
                          enctype="multipart/form-data">
                        <div class="box-body">
                            <input type="file" name="studentsFile" id="studentsFile">
                        </div>
                        <div class="box-footer with-border">
                            <button class="btn btn-primary btn-flat" type="submit" name="submit">Verzenden</button>
                        </div>
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
            <div class="col-xs-7">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Studenten</h3>
                    </div>
                    <div class="box-body">
                        <table id="studentsTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th class="noSort"></th>
                                <th>Naam</th>
                                <th>OV Nummer</th>
                                <th class="dateSort">Geboortedatum</th>
                                <th>Klas</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($students as $student)
                                <tr>
                                    <td class="checkboxStudents">
                                        <input type="checkbox" class="icheckbox_minimal-blue studentRow"
                                               data-student-id="{{ $student->id }}">
                                    </td>
                                    <td>{{ $student->name }}</td>
                                    <td>{{ $student->OV_number }}</td>
                                    @if ($student->birthday)
                                        <td>{{ \App\DateTime::formatDutch($student->birthday) }}</td>
                                    @else
                                        <td>-</td>
                                    @endif
                                    <td colspan="2">
                                        {{ $student->class_code }}
                                        <span class="btn-group btn-group-sm pull-right">
                                            <button type="button" class="btn btn-flat btn-default dropdown-toggle"
                                                    data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                            @foreach ($classStudents[$student->id] as $classStudent)
                                                    <li class="startLoadingButton">
                                                    <a href="{{ route('changeClass', ['student_id' => $student->id, 'class_id' => $classStudent['class_id']]) }}">{{ $classStudent['class_code'] }}</a>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <button type="button" class="btn btn-block btn-danger btn-flat deleteStudents">
                            Verwijder de geselecteerde student(en)
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection