@extends('layouts.master')

@section('content')
    @if(session()->has('message'))
        <div class="alert alert-success alert-dismissible roleSuccessAlert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> Success!</h4>
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Nieuw account toevoegen</h3>
                        <span class="pull-right help-block"><b class="danger-font--color">*</b> Deze velden zijn verplicht</span>
                    </div>
                    <div class="box-body">
                        <form id="addAccountForm" class="form form-horizontal" action="{{ route('adminAddAccount') }}"
                              method="POST">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-xs-12">
                                    <label for="name">Naam <b class="danger-font--color">*</b></label>
                                    <input id="name" type="text" class="form-control required empty" name="name"
                                           placeholder="Voledige naam: John Doe">
                                    <span class="alertHide danger-font--color">Dit veld is verplicht.</span>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xs-12">
                                    <label for="username">Gebruikersnaam <b class="danger-font--color">*</b></label>
                                    <input id="username" type="text" class="form-control required empty" name="username"
                                           placeholder="Initialen: jd">
                                    <span class="alertHide danger-font--color">Dit veld is verplicht.</span>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xs-12">
                                    <label for="password">Wachtwoord <b class="danger-font--color">*</b></label>
                                    <input id="password" type="password" class="form-control required empty" name="password"
                                           placeholder="Wachtwoord">
                                    <span class="alertHide danger-font--color">Dit veld is verplicht.</span>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xs-12">
                                    <label for="password">Wachtwoord bevestigen <b
                                                class="danger-font--color">*</b></label>
                                    <input id="password" type="password" class="form-control required empty"
                                           name="passwordCheck" placeholder="Wachtwoord">
                                    <span class="alertHide danger-font--color">Dit veld is verplicht.</span>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xs-12">
                                    <label for="email">E-mail <b class="danger-font--color">*</b></label>
                                    <input id="email" type="email" class="form-control required empty" name="email"
                                           placeholder="E-mail: johndoe@hotmail.com">
                                    <span class="alertHide danger-font--color">Dit veld is verplicht.</span>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xs-12">
                                    <label for="accountType">Type account <b class="danger-font--color">*</b></label>
                                    <select id="accountType" name="accountType" class="form-control required empty">
                                        @foreach($roles as $role)
                                            <option value="{{ $role->id }}">{{ $role->role_name }}</option>
                                        @endforeach
                                    </select>
                                    <span class="alertHide danger-font--color">Dit veld is verplicht.</span>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="box-footer with-border">
                                <button type="submit" class="btn btn-primary btn-flat">Verzenden</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Accounts overzicht</h3>
                    </div>
                    <div class="box-body">
                        <table id="classTable" class="table table-hover table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Naam</th>
                                <th>Gebruikersnaam</th>
                                <th>Rol</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($accountsInfo as $account)
                                <tr data-account-id="{{ $account['info']['id'] }}">
                                    <td>{{ $account['info']['name'] }}</td>
                                    <td>{{ $account['info']['username'] }}</td>
                                    <td colspan="2">
                                        {{ $account['info']['current_role_name'] }}
                                        <span class="btn-group btn-group-sm pull-right">
                                            <button type="button" class="btn btn-flat btn-default dropdown-toggle"
                                                    data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                @foreach ($account['rolesNotAssigned'] as $role)
                                                    <li class="startLoadingButton">
                                                        <a href="{{ route('changeRole', ['account_id' => $role['account_id'], 'role_id' => $role['role_id']])  }}">{{ $role['role_name'] }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection