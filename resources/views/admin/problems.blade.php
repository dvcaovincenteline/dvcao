@extends('layouts.master')

@section('content')
    <section class="content contentBackground">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Problemen</h3>
                    </div>
                    <div class="box-body">
                        <table id="problemsTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Probleem</th>
                                <th>Categorie</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($problems as $problem)
                                <tr>
                                    <td>
                                        {{ $problem->problem }}
                                    </td>
                                    <td>
                                        {{ $problem->category }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection