@extends('layouts.master')

@section('content')
    <div class="content contentBackground">
        <div class="row">
            <div class="col-md-6">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Signaal Punten</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                        class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                        class="fa fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="classList">Klas</label>
                            <select id="classList" class="form-control" style="width: 100%;">
                                <option selected="selected" disabled="disabled">Kies een klas</option>
                                @foreach($classes as $class)
                                    <option data-class-id="{{ $class->id }}">{{ $class->class_code }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="chart">
                            <canvas id="signalPointsChart"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection