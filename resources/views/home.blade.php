@extends('layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-0">
                <div class="panel panel-default">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Klassen Overzicht</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="table-responsive">
                                <table class="table no-margin">
                                    <thead>
                                    <tr>
                                        <th>Klas</th>
                                        <th>Aantal leerlingen</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($classInfo as $class)
                                        <tr>
                                            <td>
                                                <a class="startLoadingButton"
                                                   href="{{ route('classesOverviewStudents', ['class' => $class['id']]) }}">
                                                    {{ $class['class_code'] }}
                                                </a>
                                            </td>
                                            <td>{{ $class['students_amount'] }}</td>
                                            <td><span class="label label-success">{{ $class['class_status'] }}</span>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="box-footer clearfix">
                            <a href="{{ route('adminClasses') }}"
                               class="btn btn-sm btn-info btn-flat pull-left startLoadingButton">
                                Klas toevoegen
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-12">
                @include('includes.loadingGifBox')
                <div class="box box-info chart-loading">
                    <div class="box-header with-border">
                        <h3 class="box-title">Grafiek totaal aantal signaalpunten per klas</h3>
                    </div>
                    <div class="box-body">
                        <div class="chart">
                            <canvas class="signalPointsAllClasses classes-overview"
                                    id="signalPointsChart"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
