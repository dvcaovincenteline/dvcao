<div class="alert alert-success alert-dismissible classSuccessAlert alertHide">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> Success!</h4>
    Klas successvol toegevoegd!
</div>

<div class="alert alert-danger alert-dismissible classErrorAlert alertHide">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-ban"></i> Let op!</h4>
    Er is een onverwachte fout opgetreden!
</div>

<div class="alert alert-success alert-dismissible accountSuccessAlert alertHide">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> Success!</h4>
    Account is successvol aangemaakt!
</div>

<div class="alert alert-danger alert-dismissible accountErrorAlert alertHide">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-ban"></i> Let op!</h4>
    Er is een onverwachte fout opgetreden!
</div>

<div class="alert alert-danger alert-dismissible accountPasswordErrorAlert alertHide">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-ban"></i> Let op!</h4>
    Wachtwoorden komen niet overeen.
</div>

<div class="alert alert-success alert-dismissible importStudentsSuccessAlert alertHide">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> Success!</h4>
    Leerlingen zijn succesvol toegevoegd!
</div>

<div class="alert alert-success alert-dismissible deleteStudentsSuccessAlert alertHide">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> Success!</h4>
    Alle geselecteerde studenten zijn succesvol verwijderd!
</div>

<div class="alert alert-danger alert-dismissible deleteStudentsErrorAlert alertHide">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-ban"></i> Let op!</h4>
    Er is een fout opgetreden met het verwijderen van de leerlingen!
</div>