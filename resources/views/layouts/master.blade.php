<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Mano</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <link href="{{ asset ("./bower_components/AdminLTE/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet"
          type="text/css"/>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{ asset ("/bower_components/AdminLTE/dist/css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset ("/bower_components/AdminLTE/dist/css/skins/skin-blue.min.css")}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset ("/bower_components/AdminLTE/plugins/iCheck/all.css")}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset ("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css") }}"
          rel="stylesheet"/>
    <link href="{{ asset("/css/main.css") }}" rel="stylesheet" type="text/css">
    <script src="{{ asset ("/bower_components/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js") }}"></script>
    <script src="{{ asset ("/bower_components/AdminLTE/plugins/iCheck/icheck.min.js") }}"></script>
    <script src="{{ asset ("/bower_components/AdminLTE/bootstrap/js/bootstrap.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset ("/bower_components/AdminLTE/dist/js/app.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset ("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset ("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset ("/bower_components/AdminLTE/plugins/chartjs/Chart.min.js") }}"></script>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.10.16/sorting/date-eu.js"></script>
    <script src="{{ asset ("/js/app.js") }}"></script>
</head>
<body class="skin-blue">
<div class="wrapper">
    @include ('layouts.header')
    @include ('layouts.nav')
    <div class="content-wrapper">
        <section class="content-header">
        </section>
        <section class="content">
            @include('layouts.alerts')
            <img src="{{ asset ('/images/loading.gif') }}" class="loadingGif">
            @yield ('content')
        </section>
    </div>
    @include ('layouts.footer')

</div>
</body>
</html>