<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset("/images/davinci-logo.png") }}" class="img-circle"
                     alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>{{ \Illuminate\Support\Facades\Auth::user()->name }}</p>
                <a href="#">
                    <i class="fa fa-circle text-success"></i>
                    Online
                </a>
            </div>
        </div>
        <form class="sidebar-form">
            <div class="input-group">
                <input autocomplete="off" id="searchBar" type="text" class="form-control" placeholder="Zoeken"/>
                <span class="input-group-btn">
                    <a id='search-btn' class="btn btn-flat">
                        <i class="fa fa-search"></i>
                    </a>
                </span>
            </div>
            <div id="dropdown-content">

            </div>
        </form>
        <ul class="sidebar-menu">
            <li class="header">Menu</li>
            <li>
                <a href="{{ route('homeRoute') }}" class="startLoadingButton"><i class="fa fa-home"></i>Dashboard</a>
            </li>
            <li>
                <a href="{{ route('classesOverview') }}" class="startLoadingButton"><i class="fa fa-group"></i>
                    Klassen
                </a>
            </li>
            @if(\Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Enums\RolesEnum::ADMIN)
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-lock"></i>
                        Admin Paneel
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="{{ route('adminOverview') }}" class="startLoadingButton"><i
                                        class="fa fa-circle-o"></i> Overzicht</a>
                        </li>
                        <li>
                            <a href="{{ route('adminAccounts') }}" class="startLoadingButton"><i
                                        class="fa fa-circle-o"></i> Accounts</a>
                        </li>
                        <li>
                            <a href="{{ route('adminClasses') }}" class="startLoadingButton"><i
                                        class="fa fa-circle-o"></i> Klassen</a>
                        </li>
                        <li>
                            <a href="{{ route('adminStudents') }}" class="startLoadingButton"><i
                                        class="fa fa-circle-o"></i> Studenten</a>
                        </li>
                        <li>
                            <a href="{{ route('adminProblems') }}" class="startLoadingButton"><i
                                        class="fa fa-circle-o"></i> Leerproblemen</a>
                        </li>
                    </ul>
                </li>
            @endif
        </ul>
    </section>
</aside>