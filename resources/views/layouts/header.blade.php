<header class="main-header">
    <a href="{{ route("homeRoute") }}" id="manoBold" class="logo startLoadingButton">
        <b>Mano</b>
    </a>
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Navigatie</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-envelope-o"></i>
                        <span class="label label-success">?</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">U heeft {{ "[MSG_AMOUNT]" }} nieuwe berichten</li>
                        <li>
                            <ul class="menu">
                                <li class="displayName">{{ \Illuminate\Support\Facades\Auth::user()->name }}</li>
                                <a href="/profile">
                                    <div class="pull-left">
                                        <img src="{{ asset(\App\Http\Enums\PathsEnum::AVATAR) }}"
                                             class="img-circle userImage" alt="User Image"/>
                                    </div>
                                </a>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>
                        <span class="label label-warning">?</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">U heeft {{ "[NOTIFICATION_AMOUNT]" }} notificaties</li>
                        <li>
                            <ul class="menu">
                                <li>
                                    <a href="#">
                                        <i class="fa fa-users text-aqua"></i>
                                        {{ "[NOTIFICATION_MSG]" }}
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="footer">
                            <a href="#">Bekijk alle notificaties</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown tasks-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-flag-o"></i>
                        <span class="label label-danger">?</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">U heeft {{ "[TASK_AMOUNT]" }} taken</li>
                        <li>
                            <ul class="menu">
                                <li>
                                    <a href="#">
                                        <h3>
                                            {{ "[TASK_MSG]" }}
                                            <small class="pull-right">20%</small>
                                        </h3>
                                        <div class="progress xs">
                                            <div class="progress-bar progress-bar-aqua"
                                                 style="width: 20%" role="progressbar" aria-valuenow="20"
                                                 aria-valuemin="0" aria-valuemax="100">
                                                <span class="sr-only">20% Complete</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="footer">
                            <a href="#">Bekijk alle taken</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ asset(\App\Http\Enums\PathsEnum::AVATAR) }}"
                             class="user-image" alt="User Image"/>
                        <span class="hidden-xs">{{ \Illuminate\Support\Facades\Auth::user()->name }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-header">
                            <img src="{{ asset(\App\Http\Enums\PathsEnum::AVATAR) }}"
                                 class="img-circle" alt="User Image"/>
                            <p>
                                {{ \Illuminate\Support\Facades\Auth::user()->name }}
                                <small>Gebruiker sinds {{ \App\DateTime::formatDutchWithTime(\Illuminate\Support\Facades\Auth::user()->created_at) }}</small>
                            </p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="/profile" class="btn btn-default btn-flat">Profiel</a>
                            </div>
                            <div class="pull-right">
                                <a href="/logout" class="btn btn-default btn-flat">Uitloggen</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>