-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: mano
-- ------------------------------------------------------
-- Server version	5.7.14

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Persoonsgebonden buiten invloedsfeer school'),(2,'Persoonsgebonden problemen met theorie/praktijk'),(3,'Persoonsgebonden waar hulp door Service Centrum mogelijk is'),(4,'Schoolgebonden factoren'),(5,'Studie- en beroepskeuze factoren'),(6,'Arbeidsmarkt factoren'),(7,'Motivatie/gedrag waar verbetering mogelijk is'),(8,'Externe factoren');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `classes`
--

DROP TABLE IF EXISTS `classes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_code` varchar(45) NOT NULL,
  `status` varchar(45) NOT NULL DEFAULT 'School',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classes`
--

LOCK TABLES `classes` WRITE;
/*!40000 ALTER TABLE `classes` DISABLE KEYS */;
INSERT INTO `classes` VALUES (1,'LPICO15A1','school'),(2,'LPICO15A2','school'),(3,'LPICO15A3','school'),(31,'LPICO16A1','school'),(32,'LPICO16A2','school'),(33,'LPICO16A3','school'),(35,'LPICO17A1','School'),(36,'LPICO17A1','School'),(37,'LPICO17A2','School'),(38,'LPICO16A4','School'),(39,'LPICO15a4','School'),(40,'LPICO17A3','School'),(41,'LPIAO15A2','School'),(43,'LPIAO15A3','School'),(45,'LPICO15a4','School'),(46,'LPICO15a4','School'),(47,'LPICO15A5','School'),(48,'LPICO61A5','School');
/*!40000 ALTER TABLE `classes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `problems`
--

DROP TABLE IF EXISTS `problems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `problems` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `problem` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `problems`
--

LOCK TABLES `problems` WRITE;
/*!40000 ALTER TABLE `problems` DISABLE KEYS */;
INSERT INTO `problems` VALUES (1,1,'Verhuizing student en daaroor te grote geografische afstand'),(2,1,'Ziekte van student of iemand uit directe omgeving'),(3,1,'Overlijden van iemand uit directe omgeving'),(4,2,'Onvoldoende studieresultaten'),(5,2,'Studievoortgang loopt achter'),(6,2,'Praktijk problemen'),(7,2,'Vaktheorie problemen'),(8,2,'Reken problemen'),(9,2,'Taal problemen'),(10,3,'Sociaal-emotionele problemen'),(11,3,'Psychische stoornissen'),(12,3,'Leerproblemen'),(13,3,'Leerstoornis'),(14,3,'Problemen in de thuissituatie'),(15,4,'Heeft problemen met de inhoud en/of vormgeving van opleidingen'),(16,4,'Heeft problemen met docent(en)'),(17,4,'Ervaart onveiligheid op school'),(18,5,'Geeft foute studie-school-beroepskeuzebeslissingen aan'),(19,6,'Arbeidsovereenkomst door leerbedrijf opgezegd'),(20,6,'“Groenpluk”'),(21,6,'Kan geen stageadres vinden'),(22,6,'Sollicitatieplicht'),(23,7,'Komt afspraken niet na'),(24,7,'Komt vaak te laat of gaat eerder weg'),(25,7,'Is brutaal'),(26,7,'Heeft geen boeken of schoolspullen beschikbaar'),(27,7,'Is inactief tijdens de les'),(28,7,'Is snel afgeleid tijdens de les'),(29,7,'Laat ongewenst gedrag zien tijdens de les of daarbuiten'),(30,7,'Maakt geen huiswerk'),(31,7,'Meldt zich vaak ziek'),(32,7,'Spijbelt veel'),(33,8,'In aanraking met Jeugdzorg (dit leerjaar)'),(34,8,'In aanraking met Leerplicht (dit leerjaar)');
/*!40000 ALTER TABLE `problems` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `role_name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Admin'),(2,'Zorgcoördinator'),(3,'Docenten Team');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students`
--

DROP TABLE IF EXISTS `students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `students` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `OV_number` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `birthday` date DEFAULT NULL,
  `signal_points` int(11) DEFAULT '0',
  `class_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `class_id_idx` (`class_id`),
  CONSTRAINT `class_id` FOREIGN KEY (`class_id`) REFERENCES `classes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students`
--

LOCK TABLES `students` WRITE;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
INSERT INTO `students` VALUES (1,'99026400','Vincent Schaddelee','1997-08-25',1,47),(2,'99034086','Fenno Sebrechts','1999-04-16',2,2),(5,'99033883','Eddy de Lange','1999-05-12',4,2),(6,'99034334','Furkan Dogan','1999-03-15',1,1),(7,'99034164','Wessel van Zeggeren','1999-08-08',8,2),(9,'99034334','Furkan Dogan','1999-03-15',1,1),(19,'99034779','Ariaan Mans',NULL,0,2),(20,'99028838','Jamy Muilwijk',NULL,0,2),(21,'99036110','Mitchell van Potten',NULL,0,2),(22,'99032408','Lennard de Roij',NULL,0,2),(23,'99024836','Rody Ros',NULL,0,2),(24,'99034097','Mattias Suijker',NULL,0,2),(25,'99035834','Sem Zee',NULL,0,2),(26,'99035592','Remco de Zwart',NULL,0,2),(27,'99033000','Pietje Puk',NULL,0,48),(28,'99033225','Dino Bijdevier',NULL,0,2),(29,'99034935','Eline Boekweit',NULL,0,2),(30,'99032302','Sanno van der Graaf',NULL,0,2),(31,'99033550','Indigo Groenendijk',NULL,0,2),(32,'99033279','Rick de Jong',NULL,0,2),(33,'99031541','Benjamin Kemp',NULL,0,2),(34,'99034651','Dylan de Leeuw',NULL,0,2),(35,'99037054','Glenn Ligthart',NULL,0,2),(36,'99032293','Vincent van der Linden',NULL,0,2);
/*!40000 ALTER TABLE `students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_id_idx` (`role_id`),
  CONSTRAINT `role_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Boris Boterkoek','tam','test@test.nl','$2y$06$.UdBu0w2R8EnuT8dpU9Yc.rbCjJdrZkq7WIZzEXrS3RH/HrMBQZVy','EGWVIASoEbQNhB7Ibxb4wafd0bttrHlHXUpcaFSj0osHVQEcjU0tCQapugu0','2017-04-20 05:29:31','2017-04-20 05:29:31',2),(2,'Eline Boekweit','eb','99034935@mydavinci.nl','$2y$10$yctXGV2MiPYG0jmQ3lrw0etmfR1AOyD9lfjEKMVGnDLQw48.u55lK',NULL,'2017-04-20 05:31:17','2017-04-20 05:31:17',2),(3,'Jan-Willem Huisman','hjw','hjw@davinci.nl','$2a$06$h928Ryn8erfhhvooXRHcKuJleJnZTSpe1CIy/DGNbwj9WHGr4UMnW',NULL,'2017-04-20 05:31:19','2017-04-20 05:31:17',1),(4,'Vincent Schaddelee','vps','99026400@mydavinci.nl','$2a$06$0840bHZ3QP1SCpXORXDip.tQQ7FyWW5rtE4F6xVNcBYItzrN9d4K2',NULL,'2017-05-12 07:55:45','2017-05-12 07:55:45',1),(5,'Eddy de Lange','el','99033883@mydavinci.nl','$2a$06$.UdBu0w2R8EnuT8dpU9Yc.rbCjJdrZkq7WIZzEXrS3RH/HrMBQZVy',NULL,'2017-05-22 21:42:12','2017-05-22 21:42:12',1),(6,'Doris Droeftoeter','dd','doris@hotmail.com','$2y$10$1L73AzaEZPPqVr7rb1gtJOtMRO09pxrO82Uga79xtAzVIVX.HeG4m',NULL,'2017-09-06 10:32:05','2017-09-06 10:32:05',2),(7,'Ariaan Mans','am','ariaanmans@hotmail.com','$2y$10$czJqckioa.dHXhyYRyaH9.hwABDPRLE8sS3ksmO/VYrlABriBraxC',NULL,'2017-09-06 10:36:01','2017-09-06 10:36:01',2),(8,'Arend Adelaar','aa','arendadelaar@hotmail.com','$2y$10$Zr2E8WzTSUNCqev5QWZXJ.rbfK51V8a777W3DyjLsS8OEqnUrH02e',NULL,'2017-09-08 05:57:26','2017-09-08 05:57:26',1),(12,'Arnold de Jong','jga','jga@davinci.nl','$2a$06$vaeN4C2xUOTlrm1anBcYh.aHK2viDBpqMpM1ronbGssNACdGEjDia',NULL,'2017-09-15 07:06:27','2017-09-15 07:06:27',1),(13,'Jeroen Slemmer','slr','slr@davinci.nl','$2y$10$avAQnutNrU.ni5rJMBmck.9Z0CJOtwoLTeO0I/LI.RypsNIS61BGK',NULL,'2017-09-29 09:55:50','2017-09-29 09:55:50',3),(14,'<script>alert(document.cookie)</script>','vps','vps@vps.nl','$2y$10$FAdddlqyIEPDjaa4Sm9RxO1omUd753t.x.ByVXNpVw3m6AsZ3.2ey',NULL,'2017-11-15 07:46:21','2017-11-15 07:46:21',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-01  9:32:12
