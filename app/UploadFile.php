<?php

namespace App;


use App\Http\Models\Students;
use Maatwebsite\Excel\Collections\RowCollection;
use Maatwebsite\Excel\Facades\Excel;

class UploadFile extends RowCollection
{

    public static function importStudents($request)
    {
        $headers = [];
        $newArray = (object)[];
        $index = 0;
        $file = $request->file('studentsFile');
        $file->move(storage_path('imports'), 'import.' . $file->getClientOriginalExtension());

        Excel::load(storage_path('imports/import.xlsx'), function ($reader) use ($headers, $newArray, $index) {
            $result = $reader->get();
            foreach ($result->heading as $headerValue) {
                $headers[] = $headerValue;
            }
            foreach ($result->items as $header => $value) {
                $keyValue = 0;
                foreach ($value->items as $item) {
                    $newArray->items[$headers[$keyValue]] = $item;
                    $keyValue++;
                }
                unset($result->items[$index]->items);
                $result->items[$index]->items = $newArray->items;
                $index++;
            }
            $result->heading = array_replace([$result], $headers);
            foreach ($result->items as $student) {
                Students::insertStudents($student->items);
            }
        });
    }

}
