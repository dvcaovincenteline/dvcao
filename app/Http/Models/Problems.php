<?php

namespace App\Http\Models;

use Illuminate\Support\Facades\DB;

class Problems
{
    const TABLENAME = 'problems';

    public static function getAllProblemsWithCategories()
    {
        return DB::table(self::TABLENAME)
            ->join('categories', 'categories.id', 'problems.category_id')
            ->select('problems.problem', 'categories.category')
            ->get();
    }

}