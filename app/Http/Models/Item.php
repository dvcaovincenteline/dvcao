<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{

    public $fillable = ['OV_number', 'name', 'class_id'];

}