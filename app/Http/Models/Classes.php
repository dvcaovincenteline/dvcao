<?php

namespace App\Http\Models;


use Illuminate\Support\Facades\DB;

class Classes
{

    const TABLENAME = "classes";

    public static function addClass($request)
    {
        DB::table(self::TABLENAME)->insert(['class_code' => $request->className]);
    }

    public static function getInfo($classId)
    {
        $classInfo = [];
        $signalPointsTotal = 0;
        $studentsAmount = count(Students::getAllInClass($classId));
        $signalPoints = Students::getSignalPoints($classId);
        foreach( $signalPoints as $signalPoint) {
            $signalPointsTotal = $signalPointsTotal + $signalPoint->signal_points;
        }
        $class = DB::table(self::TABLENAME)->where('id', $classId)->first();

        $classInfo['id'] = $class->id;
        $classInfo['class_code'] = $class->class_code;
        $classInfo['students_amount'] = $studentsAmount;
        $classInfo['class_status'] = ucwords($class->status);
        $classInfo['signal_points'] = $signalPointsTotal;

        return $classInfo;
    }

    public static function getClassByCode($classCode)
    {
        return DB::table(self::TABLENAME)->where('class_code', $classCode)->first();
    }

    public static function getAllClassesOrdered()
    {
        return DB::table(self::TABLENAME)->orderBy('class_code', 'asc')->get();
    }

    public static function getAllSignalPoints()
    {
        return DB::table(self::TABLENAME)->select('class_code', 'signal_points')->get();
    }

}

