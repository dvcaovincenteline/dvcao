<?php

namespace App\Http\Models;

use App\DateTime;
use Illuminate\Support\Facades\DB;

class Students
{

    const TABLENAME = 'students';

    public static function getAll()
    {
        $students = DB::table(self::TABLENAME)->get();
        foreach ($students as $student) {
            $student->birthday = DateTime::formatDutch($student->birthday);
        }

        return $students;
    }

    public static function getSignalPoints($classId)
    {
        return DB::table(self::TABLENAME)->where('class_id', $classId)->select('name', 'signal_points')->get();
    }

    public static function getAllInClass($classId)
    {
        return DB::table(self::TABLENAME)->where('class_id', $classId)->get();
    }

    public static function getAllWithClassName($classId)
    {
        $studentInfo = [];
        $students = DB::table(self::TABLENAME)
            ->join('classes', 'classes.id', 'students.class_id')
            ->where('students.class_id', $classId)
            ->select('students.name', 'classes.class_code')
            ->get();

        foreach ($students as $student) {
            $name = explode(' ', $student->name, 2);
            $studentInfo[] = [
                'first_name' => $name[0],
                'last_name' => $name[1],
                'class_code' => $student->class_code
            ];
        }

        return $studentInfo;

    }

    public static function insertStudents($student)
    {
        $classId = Classes::getClassByCode($student['class_code'])->id;
        if (!count(self::getByOvNumber($student['ov_number']))) {
            DB::table(self::TABLENAME)->insert([
                'OV_Number' => (string)$student['ov_number'],
                'name' => $student['name'],
                'class_id' => $classId
            ]);
        }
    }

    public static function changeClass($studentId, $classId)
    {
        DB::table(self::TABLENAME)->where('id', $studentId)->update(['class_id' => $classId]);
    }

    public static function getByOvNumber($studentOvNumber)
    {
        return DB::table(self::TABLENAME)->where('OV_number', $studentOvNumber)->first();
    }

    public static function deleteStudents($request)
    {
        foreach ($request->studentsToDelete as $studentId) {
            DB::table(self::TABLENAME)->where('id', $studentId)->delete();
        }
    }
}