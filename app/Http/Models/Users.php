<?php

namespace App\Http\Models;

use Illuminate\Support\Facades\DB;

class Users
{
    const TABLENAME = 'users';

    public static function insert($data)
    {
        DB::table(self::TABLENAME)->insert([
            'name' => $data->name,
            'username' => $data->username,
            'email' => $data->email,
            'password' => bcrypt($data->password),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'role_id' => $data->accountType
        ]);
    }

    public static function changeRole($userId, $roleId)
    {
        DB::table(self::TABLENAME)->where('id', $userId)->update(['role_id' => $roleId]);
    }
}