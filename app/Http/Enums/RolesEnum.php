<?php

namespace App\Http\Enums;

class RolesEnum
{
    const ADMIN = 1;
    const ZORG = 2;
    const DOCENT = 3;
}