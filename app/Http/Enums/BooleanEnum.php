<?php

namespace App\Http\Enums;


class BooleanEnum
{
    const TRUE = 1;
    const FALSE = 0;

}