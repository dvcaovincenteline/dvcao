<?php

namespace App\Http\Controllers;

use App\Http\Models\Classes;
use App\Http\Models\Database;
use App\Http\Models\Students;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class HomeController extends Controller
{
    /** Create a new controller instance. */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return View
     */
    public function index()
    {
        $classes = Database::getAll(Classes::TABLENAME);
        $classInfo = [];
        $students = [];
        foreach ($classes as $class) {
            $classInfo[] = Classes::getInfo($class->id);
            $students[] = Students::getAllWithClassName($class->id);
        }
        asort($classInfo);
        if (Auth::check()) {
            View::share('classInfo', $classInfo);
            View::share('students', $students);
            return view('home');
        } else {
            return view('auth/login');
        }
    }

    public function classesOverviewIndex()
    {
        $classes = Database::getAll(Classes::TABLENAME);
        foreach ($classes as $class) {
            $classInfo[$class->class_code] = Classes::getInfo($class->id);
            $classInfo[$class->class_code]['id'] = $class->id;
        }
        asort($classInfo);
        View::share('classInfo', $classInfo);
        return view('classes/classesOverview');
    }

    public function classesOverviewStudentsIndex($classId)
    {
        if (is_numeric($classId) && $classId > 0) {
            $students = Students::getAllInClass($classId);
            $className = Database::getOneById(Classes::TABLENAME, $classId)->class_code;
            View::share('students', $students);
            View::share('className', $className);
            return view('classes/classStudentsOverview');
        }
        return redirect('/home');
    }

    public function getDataSearch()
    {
        $dataSearch = [];
        $dataSearch['classes'] = Database::getSpecificColumn(Classes::TABLENAME, 'class_code');
        $dataSearch['ovNumbers'] = Database::getSpecificColumn(Students::TABLENAME, 'OV_Number');
        $dataSearch['names'] = Database::getSpecificColumn(Students::TABLENAME, 'name');
        return new JsonResponse($dataSearch);
    }
}
