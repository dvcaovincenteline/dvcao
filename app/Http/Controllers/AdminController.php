<?php

namespace App\Http\Controllers;

use App\Http\Models\Classes;
use App\Http\Models\Database;
use App\Http\Models\Roles;
use App\Http\Models\Students;
use App\Http\Models\Users;
use App\UploadFile;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Http\Models\Problems;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }

    public function indexProblems()
    {
        $problems = Problems::getAllProblemsWithCategories();

        View::share('problems', $problems);
        return view('admin/problems');
    }

    public function indexStudents()
    {
        $students = Database::getAll(Students::TABLENAME);
        $classes = Classes::getAllClassesOrdered();
        $classStudents = [];
        foreach ($students as $student) {
            foreach ($classes as $class) {
                if ($student->class_id == $class->id) {
                    $student->class_code = $class->class_code;
                } else {
                    $classStudents[$student->id][] = [
                        'student_id' => $student->id,
                        'class_id' => $class->id,
                        'class_code' => $class->class_code
                    ];
                }
            }
        }

        View::share('classStudents', $classStudents);
        View::share('students', $students);
        return view('admin/students');
    }

    public function indexOverview()
    {
        $classes = Database::getAll(Classes::TABLENAME);

        View::share('classes', $classes);
        return view('admin/overview');
    }

    public function indexClasses()
    {
        $classes = Database::getAll(Classes::TABLENAME);
        foreach ($classes as $class) {
            $classInfo[$class->class_code] = Classes::getInfo($class->id);
            $classInfo[$class->class_code]['id'] = $class->id;
        }
        asort($classInfo);
        View::share('classInfo', $classInfo);
        return view('admin/classes');
    }

    public function indexAccounts()
    {
        $accountsInfo = [];
        $accounts = Database::getAll(Users::TABLENAME);
        $roles = Database::getAll(Roles::TABLENAME);
        foreach ($accounts as $account) {
            $rolesLeft = Roles::getAllLeft($account->role_id);
            $accountsInfo[$account->id]['info'] = [
                'id' => $account->id,
                'name' => $account->name,
                'username' => $account->username,
                'current_role_name' => Roles::getRole($account->role_id)->role_name
            ];
            foreach ($rolesLeft as $roleLeft) {
                $accountsInfo[$account->id]['rolesNotAssigned'][] = [
                    'account_id' => $account->id,
                    'role_id' => $roleLeft->id,
                    'role_name' => $roleLeft->role_name
                ];
            }
        }
        View::share('accountsInfo', $accountsInfo);
        View::share('roles', $roles);
        return view('admin/accounts');
    }

    public function getSignalPoints($classId)
    {
        return new JsonResponse(Students::getSignalPoints($classId));
    }

    public function getAllSignalPoints()
    {
        $classes = Database::getAll(Classes::TABLENAME);
        foreach ($classes as $class) {
            $classInfo[] = Classes::getInfo($class->id);
            $classInfo[]['id'] = $class->id;
        }
        asort($classInfo);
        return new JsonResponse($classInfo);
    }

    public function addClass(Request $request)
    {
        if (typeOf($request->className) !== 'undefined') {
            Classes::addClass($request);
            return new JsonResponse(true);
        } else {
            return new JsonResponse(false);
        }
    }

    public function importStudents(Request $request)
    {
        UploadFile::importStudents($request);
        return back()->with('message', 'Leerlingen succesvol toegevoegd');
    }

    public function addAccount(Request $request)
    {
        if (!empty($request->username) && !empty($request->password) && !empty($request->passwordCheck) && !empty($request->email)) {
            if ($request->password === $request->passwordCheck) {
                Users::insert($request);
                return new JsonResponse(true);
            } else {
                return new JsonResponse('password');
            }
        } else {
            return new JsonResponse(false);
        }
    }

    public function changeRole($userId, $roleId)
    {
        $user = Database::getOneById(Users::TABLENAME, $userId);
        $role = Database::getOneById(Roles::TABLENAME, $roleId);
        Users::changeRole($userId, $roleId);
        return back()->with('message', 'Rol van ' . $user->name . ' is succesvol aangepast naar ' . $role->role_name);
    }

    public function changeClass($studentId, $classId)
    {
        $class = Database::getOneById(Classes::TABLENAME, $classId);
        $student = Database::getOneById(Students::TABLENAME, $studentId);
        Students::changeClass($studentId, $classId);
        return back()->with('message', $student->name . ' is succesvol verplaats naar klas ' . $class->class_code);
    }

    public function deleteStudents(Request $request)
    {
        Students::deleteStudents($request);
        return new JsonResponse(true);
    }
}
