<?php

use Illuminate\Support\Facades\Auth;

Auth::routes();

$this->get('/', 'HomeController@index');
$this->get('/home', 'HomeController@index')->name('homeRoute');
$this->get('/logout', 'Auth\LoginController@logout');

$this->group(['prefix' => 'classes'], function () {
    $this->get('overview', 'HomeController@classesOverviewIndex')->name('classesOverview');
    $this->get('overview/{classId}', 'HomeController@classesOverviewStudentsIndex')->name('classesOverviewStudents');
});

$this->group(['prefix' => 'searches'], function () {
    $this->get('dataSearch', 'HomeController@getDataSearch')->name('getDataSearch');
});

$this->group(['prefix' => 'upload'], function () {
    $this->post('importStudents', 'AdminController@importStudents')->name('importStudentsUpload');
});

$this->group(['prefix' => 'admin', 'middleware' => 'admin'], function () {
    $this->get('problems', 'AdminController@indexProblems')->name('adminProblems');
    $this->get('students', 'AdminController@indexStudents')->name('adminStudents');
    $this->get('overview', 'AdminController@indexOverview')->name('adminOverview');
    $this->get('classes', 'AdminController@indexClasses')->name('adminClasses');
    $this->get('accounts', 'AdminController@indexAccounts')->name('adminAccounts');
    $this->get('getSignalPoints/{classId}', 'AdminController@getSignalPoints');
    $this->post('addClass', 'AdminController@addClass');
    $this->post('addAccount', 'AdminController@addAccount')->name('adminAddAccount');
    $this->get('changeRole/{accountId}/{roleId}', 'AdminController@changeRole')->name('changeRole');
    $this->get('changeClass/{studentId}/{classId}', 'AdminController@changeClass')->name('changeClass');
    $this->get('getAllSignalPoints', 'AdminController@getAllSignalPoints');
    $this->get('deleteStudents', 'AdminController@deleteStudents')->name('deleteStudents');
});
